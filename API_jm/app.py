from flask import Flask, request, jsonify, redirect
from flask_cors import CORS, cross_origin
import sqlite3
import json

app = Flask(__name__)

@app.route('/api/profiles', methods=['GET', 'POST'])
@cross_origin()
def collection():
    if request.method == 'GET':
        all_profiles = get_all_profiles()
        return json.dumps(all_profiles)
    elif request.method == 'POST':
        data = request.form
        result = add_profile(data['cP_name'])
        return redirect('http://localhost:3000/home')

@app.route('/api/profile/<profile_id>', methods=['GET', 'PUT', 'DELETE'])
@cross_origin()
def resource(profile_id):
    if request.method == 'GET':
        profile =  get_single_profile(profile_id)
        return json.dumps(profile)
    elif request.method == 'PUT':
        data = request.form
        result = edit_profile(profile_id, data['cP_name'], data['cP_block1'])
        return redirect('http://localhost:3000/')
    elif request.method == 'DELETE':
        result = delete_profile(profile_id)
        return redirect('http://localhost:3000/')

def add_profile(name, block1, block2, block3, block4):
    try:
        with sqlite3.connect('compagnyProfile.db') as connection:
            cursor = connection.cursor()
            cursor.execute("""
                INSERT INTO compagyProfile (cP_name) values (?);
                """, (name,))
            result = {'status': 1, 'message': 'profile Added'}
    except:
        result = {'status': 0, 'message': 'error'}
    return result

def get_all_profiles():
    with sqlite3.connect('compagnyProfile.db') as connection:
        cursor= connection.cursor()
        cursor.execute("SELECT * FROM compagnyProfile ORDER BY id desc")
        all_profiles = cursor.fetchall()
        return all_profiles

def get_single_profile(profile_id):
    with sqlite3.connect('compagnyProfile.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM compagnyProfile WHERE id = ?", (profile_id))
        profile = cursor.fetchone()
        return profile

def edit_profile(name, block1, block2, block3, block4):
    try:
        with sqlite3.connect('compagnyProfile.db') as connection:
            connection.execute("UPDATE compagnyProfile SET cP_name = ?, cP_block1 = ?, cP_block2 = ?, cP_block3 = ?, cP_block4 = ? WHERE ID = ?;",
                    (name, block1, block2, block3, block4))
            result = {'status': 1, 'message': 'profile Added'}
    except:
        result = {'status': 0, 'message': 'error'}
    return result

def delete_profile(profile_id):
    try:
        with sqlite3.connect('compagnyProfile.db') as connection:
            connection.execute("DELETE FROM compagnyProfile WHERE ID = ?;", (profile_id))
            result = {'status': 1, 'message': 'PROFILE Deleted'}
    except:
        result = {'status': 0, 'message': 'Error'}
    return result

if __name__ == '__main__':
    app.debug = True
    app.run()
