import sqlite3

def drop_table_users():
    with sqlite3.connect('users.db') as connection:
        c = connection.cursor()
        c.execute("""DROP TABLE IF EXISTS users;""")
    return True

def drop_table_compagnyProfile():
    with sqlite3.connect('compagnyProfile.db') as connection:
        c = connection.cursor()
        c.execute("""DROP TABLE IF EXISTS compagnyProfile;""")
    return True

def create_db_users():
    with sqlite3.connect('users.db') as connection:
        c = connection.cursor()
        table = """CREATE TABLE users(
            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_email VARCHAR(65) NOT NULL,
            user_name VARCHAR(65) NOT NULL,
            user_password VARCHAR(65) NOT NULL
        );
        """
        c.execute(table)
    return True

def create_db_compagnyProfile():
    with sqlite3.connect('compagnyProfile.db') as connection:
        c = connection.cursor()
        table = """CREATE TABLE compagnyProfile(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            cP_name VARCHAR(65) NOT NULL
        );
        """
        c.execute(table)
    return True

if __name__ == '__main__':
    drop_table_users()
    create_db_users()
    drop_table_compagnyProfile()
    create_db_compagnyProfile()